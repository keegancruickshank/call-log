const { app, BrowserWindow } = require('electron');
const remote = require('electron').remote

var cloudData
var noticeTimer
var editing = false
var editingid

document.getElementById("loader").style.display = "block";
document.getElementById("empty-file").style.display = "none";
document.getElementById("main-app").style.display = "none";
document.getElementById("table").style.display = "none";

//Database Ref
var database = firebase.database()

//Snapshot Updates Data
var callRef = firebase.database().ref('/calls')
callRef.on('value', function(snapshot) {
  cloudData = snapshot.val()
  sortData()
  setDefaults()
});

document.getElementById("editButtonAdd").style.display = "none";
document.getElementById("addButtonAdd").style.display = "none";

function setDefaults() {
  document.addEventListener('keydown', event => {
    if(event.code == "F1") {
      addNew()
    }
  })
  document.addEventListener('keydown', event => {
    if(event.code == "Enter") {
      addAddress()
    }
  })
  var table = document.getElementById("table-body")
  if (cloudData) {
    table.innerHTML = "";
    for(let i = 0; i < cloudData.length; i++) {
      var caller = cloudData[i]["caller"]
      var site = cloudData[i]["location"]
      var problem = cloudData[i]["problem"]
      var staff = cloudData[i]["staff"]
      var time = cloudData[i]["time"]

      var tr = document.createElement('tr');
      tr.id = "session-" + i
      var td1 = document.createElement('td');
      td1.appendChild(document.createTextNode(caller))
      td1.id = "caller-" + i
      var td2 = document.createElement('td');
      td2.appendChild(document.createTextNode(site))
      td2.id = "site-" + i
      var td3 = document.createElement('td');
      td3.appendChild(document.createTextNode(problem))
      td3.id = "problem-" + i
      var td4 = document.createElement('td');
      td4.appendChild(document.createTextNode(staff))
      td4.id = "staff-" + i
      var td5 = document.createElement('td');
      td5.appendChild(document.createTextNode(time))
      td5.id = "time-" + i
      var td6 = document.createElement("td");
      td6.innerHTML = '<button type="button" onclick="editItem(' + i + ')" class="btn btn-warning"><i id="' + "delete-" + i + '" class="fa fa-cogs"></i></button><button type="button" onclick="deleteItem(' + i + ')" class="btn btn-danger"><i id="' + "delete-" + i + '" class="fa fa-trash-o"></i></button>';
      tr.appendChild(td1)
      tr.appendChild(td2)
      tr.appendChild(td3)
      tr.appendChild(td4)
      tr.appendChild(td5)
      tr.appendChild(td6)
      table.appendChild(tr)
      document.getElementById("empty-file").style.display = "none"
    }
    document.getElementById("loader").style.display = "none";
    document.getElementById("empty-file").style.display = "none";
    document.getElementById("main-app").style.display = "block";
    document.getElementById("table").style.display = "table";
  } else {
    document.getElementById("loader").style.display = "none";
    document.getElementById("empty-file").style.display = "flex";
    document.getElementById("main-app").style.display = "block";
    document.getElementById("table").style.display = "none";
  }
}

function sortData() {
  if (cloudData) {
    cloudData.sort(function(a, b) {
      a = new Date(a["time"]);
      b = new Date(b["time"]);
      return a>b ? -1 : a<b ? 1 : 0;
    });
  }
}

function addAddress() {
  var caller = document.getElementById("caller").value
  var site = document.getElementById("site").value
  var problem = document.getElementById("problem").value
  var staff = "Keegan";
  if(caller != "" && site != "" && problem != "" && staff != "") {
    var time
    if (editing) {
      time = document.getElementById("edit-time").value
      cloudData[editingid] = {
        "caller": caller,
        "location": site,
        "problem": problem,
        "staff": staff,
        "time": time
      }
    } else {
      time = new Date().toLocaleString();
      if (cloudData) {
        cloudData.push({
          "caller": caller,
          "location": site,
          "problem": problem,
          "staff": staff,
          "time": time
        })
      } else {
        cloudData = [{
          "caller": caller,
          "location": site,
          "problem": problem,
          "staff": staff,
          "time": time
        }]
      }
    }
    writeData()
    document.getElementById("caller").value = "";
    document.getElementById("site").value = "";
    document.getElementById("problem").value = "";
    document.getElementById("edit-time").value = time = new Date().toLocaleString();
    closeAdd()
    editing = false;
    document.getElementById("editButtonAdd").style.display = "none";
    document.getElementById("addButtonAdd").style.display = "none";
  }
}

function writeData() {
  firebase.database().ref('/calls').set(cloudData, function(error) {
    if (error) {
      showAlert("danger", "Unable to save data")
    } else {
      showAlert("success", "Call log edited successfully")
    }
  });
}

function closeApp () {
  let w = remote.getCurrentWindow()
  w.close()
}

function maximize() {
  let w = remote.getCurrentWindow()
  if(w.isMaximized()){
      w.restore();
  } else {
      w.maximize();
  }
}

function minimizeApp() {
  let w = remote.getCurrentWindow()
  w.minimize();
}

function maximizeApp() {
  let w = remote.getCurrentWindow()
  if(w.isMaximized()){
      w.restore();
  } else {
      w.maximize();
  }
}

function editItem(i) {
  document.getElementById("editButtonAdd").style.display = "inline-block";
  document.getElementById("addButtonAdd").style.display = "none";
  editing = true;
  editingid = i;
  document.getElementById("add-container").style.display = "flex";
  document.getElementById("caller").value = cloudData[i]["caller"]
  document.getElementById("site").value = cloudData[i]["location"]
  document.getElementById("problem").value = cloudData[i]["problem"]
  document.getElementById("staff").value = cloudData[i]["staff"]
  document.getElementById("edit-time").value = cloudData[i]["time"]
}

function saveEdit() {
  addAddress()
}

function deleteItem(i) {
  cloudData.splice(i, 1);
  if (cloudData.length == 0) {
    document.getElementById("empty-file").style.display = "none";
    document.getElementById("main-app").style.display = "none";
  }
  writeData()
  showAlert("success", "Delete Successful")
}

function addNew() {
  document.getElementById("editButtonAdd").style.display = "none";
  document.getElementById("add-container").style.display = "flex";
  document.getElementById("addButtonAdd").style.display = "inline-block";
  document.getElementById("caller").focus();
}

function closeAdd() {
  editing = false
  document.getElementById("add-container").style.display = "none";
}

function showAlert(type, message) {
  if (noticeTimer != undefined) {
    clearTimeout(noticeTimer)
  }
  document.getElementById("notice").innerHTML = '<div class="alert alert-' + type + '" role="alert">' + message + '</div>'
  noticeTimer = setTimeout(function () {
    document.getElementById("notice").innerHTML = ""
  }, 3000)
}
